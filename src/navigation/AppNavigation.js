import React from "react";
import { StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import AwesomeIcon from "react-native-vector-icons/FontAwesome";
import Home from "../Screens/Home";
import Cart from "../Screens/Cart";
import Account from "../Screens/Account";
import Ticket from "../Screens/Ticket";
import Picking from "../Screens/Packing";
import colors from "../styles/colors";

const Tab = createMaterialBottomTabNavigator();

export default function AppNavigation() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        barStyle={styles.navigation}
        screenOptions={({ route }) => ({
          tabBarIcon: (routeStatus) => {
            return setIcon(route, routeStatus);
          },
        })}
      >
        <Tab.Screen
          name="home"
          component={Ticket}
          options={{
            title: "Inicio",
          }}
        />
          <Tab.Screen
          name="cart"
          component={Cart}
          options={{
            title: "Carrito",
          }}
        />
       
       <Tab.Screen
          name="account"
          component={Account}
          options={{
            title: "Account",
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

function setIcon(route, routeStatus) {

  let iconName = "";
  switch (route.name) {
    case "home":
      iconName = "home";
      break;
    case "cart":
      iconName = "shopping-cart";
      break;
    case "account":
      iconName = "user";
      break;

    default:
      break;
  }
  return <AwesomeIcon name={iconName} style={[styles.icon]} />;
}

const styles = StyleSheet.create({
  navigation: {
    backgroundColor: colors.bgDark,
  },
  icon: {
    fontSize: 20,
    color: colors.fontLight,
  },
});
