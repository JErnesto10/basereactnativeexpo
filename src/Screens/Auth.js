import React, {useState} from 'react'
import { StyleSheet,View, Text, KeyboardAvoidingView } from 'react-native'
import RegisterForm from '../components/Auth/RegisterForm';
import LoginForm from '../components/Auth/LoginForm';
import {LayoutStyle} from"../styles/layout";


export default function Auth() {
const [showLogin, setShowLogin] = useState(true);

const changeForm = () => setShowLogin(!showLogin);

    return (
        <View style={styles.container}>  
          <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"}>
            {showLogin ? <LoginForm changeForm={changeForm}/> : <RegisterForm changeForm={changeForm} />}
          </KeyboardAvoidingView>
        </View>
    )
}


const styles = StyleSheet.create({
    logo: {
      width: "100%",
      height: 50,
      resizeMode: "contain",
      marginBottom: 20,
    },
    container: {
      padding: 10,
      paddingBottom: 0,
    },
  });