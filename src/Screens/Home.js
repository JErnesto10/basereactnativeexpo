import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import Pendientes from './Rutero/Pendientes';
import Entregados from './Rutero/Entregados';
import Pagados from './Rutero/Pagados';

const Stack = createStackNavigator();
const Tab = createMaterialTopTabNavigator();


function TabStack() {
    return (
      <Tab.Navigator
        initialRouteName="Feed"
        tabBarOptions={{
          activeTintColor: 'orange',
          inactiveTintColor: '#F8F8F8',
          pressColor: 'white',
          style: {
            backgroundColor: '#242424',
          },
          labelStyle: {
            textAlign: 'center',
          },
          indicatorStyle: {
            borderBottomColor: '#87B56A',
            borderBottomWidth: 2,
          },
        }}>
        <Tab.Screen
          name="Pendientes"
          component={Pendientes}
          options={{
            tabBarLabel: 'Pendientes',
            //tabBarIcon: ({ color, size }) => (
            //<MaterialCommunityIcons name="home" color={color} size={size} />
            //),
          }}  />
        <Tab.Screen
          name="Entregados"
          component={Entregados}
          options={{
            tabBarLabel: 'Entregados', 
            // tabBarIcon: ({ color, size }) => (
            //   <MaterialCommunityIcons name="settings" color={color} size={size} />
            // ),
          }} />
          <Tab.Screen
          name="Pagados"
          component={Pagados}
          options={{
            tabBarLabel: 'Pagados',
            // tabBarIcon: ({ color, size }) => (
            //   <MaterialCommunityIcons name="settings" color={color} size={size} />
            // ),
          }} />
      </Tab.Navigator>
    );
  }

export default function Home() {
    return (
      <NavigationContainer independent={true}>
      <Stack.Navigator
        initialRouteName="Settings"
        screenOptions={{
          headerStyle: { backgroundColor: '#242424' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' }
        }}>
        <Stack.Screen name="TabStack" component={TabStack} options={{ title: 'Pedidos' }}/>
      </Stack.Navigator>
    </NavigationContainer>
    )
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent:"center",
        alignItems: "center",
    }
})