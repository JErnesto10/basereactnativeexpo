import React,{useState} from 'react'
import { View, Text, StyleSheet,ScrollView } from 'react-native'
import { Button } from "react-native-paper";
import DataTable, {COL_TYPES} from 'react-native-datatable-component';
//import {Collapse,CollapseHeader, CollapseBody, AccordionList} from 'accordion-collapse-react-native';


export default function  Packing() {

    const lstPacking =[
        { nombre: 'TOZINETE AHUMADO REB 1 KG', precio: 60, cantidad: '20KG', UM:'KG',IVA:0,Subtotal:'50'},
        { nombre: 'RTE. DE COSTILLA ESPECIAL KE', precio: 68, cantidad: '3kg', UM:'KG',IVA:0,Subtotal:'40'},
        { nombre: 'PIERNA 1KG', precio: 90, cantidad: '1KG', UM:'KG',IVA:0,Subtotal:'10'}
    ]

    const nameOfCols = ['nombre', 'precio', 'cantidad','UM','IVA','Subtotal'];

  return(
    <View style={styles.container}>
    <Text style={{ fontSize: 15, alignSelf: "center", paddingBottom:10 }}>Packing List</Text>
    <ScrollView horizontal={true}>
        
    <DataTable style={{color:'red'}}
            data={lstPacking} 
            colNames={nameOfCols}
            noOfPages={1}
            style={styles.headerColor}
        />
    </ScrollView>
    </View>
  )
};


const styles = StyleSheet.create({
 
    container: {
        paddingTop: 50,
        paddingHorizontal: 15,
        
      },
      headerColor:{
        backgroundColor:'#ff6900',
        color:'white'
      }
 
});