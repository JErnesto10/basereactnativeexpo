import React,{useState,useEffect} from 'react'
import { View, Text,StyleSheet,TextInput,Picker } from 'react-native'
import { Button,DataTable  } from "react-native-paper";
import * as Print from 'expo-print';
import { shareAsync } from 'expo-sharing';


export default function Ticket(props) {
  const {changeForm} = props;
  const [loading, setLoading] = useState(false);
  const [selectedPrinter, setSelectedPrinter] = React.useState();
  const [importePago, setImportePago] =  useState(0);
  const [importeRestante, setImporteRestante] =  useState(0);
  const [selectedPago, setSelectFormaPago] = useState("Efectivo");
  
  const data ={
      requiereFactura:'No',
      tipoVenta:'Contado',
      formaPago:'Efectivo',
      importeCobrar: 103,
      importePagar:120,
      importeRestante:37,
      lstData:[{"id":1,"producto":"producto1","cantidad":1.00,"importe":2.00,"precio":25.00},
      {"id":2,"producto":"producto2","cantidad":3.00,"importe":8.00,"precio":145.00},
      {"id":3,"producto":"producto3","cantidad":7.00,"importe":56.00,"precio":3200.00}]
  }
  //Restante es la feria que se va regresar al cliente
  const restante= importePago-data.importeCobrar;

  const [currentDate, setCurrentDate] = useState('');

  useEffect(() => {

    //Obtener la hora y fecha Actual
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds
    setCurrentDate(
      date + '/' + month + '/' + year 
      + ' ' + hours + ':' + min + ':' + sec
    );
  }, []);

  //Iteracion de productos quizas haya una mejor forma de resolverlo gg
    var forProductos= "";
    var forCantidad= "";
    var forImporte= "";
    var forPrecio= "";

    for (let i = 0; i < data.lstData.length; i++) {
      forProductos += `<li>${data.lstData[i].producto}</li>`;
      forCantidad += `<li>${data.lstData[i].cantidad}</li>`;
      forImporte += `<li>$ ${data.lstData[i].importe}</li>`;
      forPrecio += `<li>$ ${data.lstData[i].precio}</li>`;

    }
  //Fin iteracion productos

  //Variable html es la que imprime el ticket de forma nativa
  var html = `
<html>
  <head>
  <img
  src="https://www.kowi.com.mx/wp-content/uploads/elementor/thumbs/04_LOGO_KOWI_FOOTER-otrq5nvcy2ogcz0in6uw7l9j5y30yk5vfxkj3ica2g.png"
  style="width: 80vw;" />
  </head>
  <body style="text-align: center;">
      <div style="width: 100%; max-width: 100%;">
     <br>
     <br>
        <label style="font-size: 23px;">ALIMENTOS KOWI SA DE CV AKO-971007-558</label>
     <br>
        <label style="font-size: 23px;">MATRIX: CARRETERA FEDERAL MEXICO-NOGALES KM 178 7 NAVOJOA, SONORA C.CP 85230</label>
     <br>
      <label style="font-size: 23px;">EXPEDIDO EN:HIRIBIS #1422</label>
      <br>
        <label style="font-size: 23px;">NOMBRE AGENTE: REPARTO OBREGON RUTA 5</label>   
      <br>
        <label style="font-size: 23px;">Carretera Internacional Km 1845 Zona Industrial "2 Bodega #5</label>
      <br>
        <label style="font-size: 23px;">Ubicado en Carlos Raygoza s/n entre Mandarina y Jamaica, Central de Abastos C.P 85065</label>
      <br>
        <label style="font-size: 23px;">CUIDAD:OBREGON</label>
      <br>
        <label style="font-size: 23px;">CLIENTE: ABARROTES MELISA</label>
      <br>
      <br>
      <br>
      <table style="border-top: 1px solid black; border-collapse: collapse;">
          <thead>
              <tr>
                  <th style=" width: 400px; max-width: 400px;word-break: break-all; font-size: 23px; border-top: 1px solid black ">Producto</th>
                  <th style="width: 200px; max-width: 200px; font-size: 23px; border-top: 1px solid black">Cantidad</th>
                  <th style="width: 200px; max-width: 200px; word-break: break-all; font-size: 23px; border-top: 1px solid black">Importe</th>
                  <th style="width: 200px; max-width: 200px; word-break: break-all; font-size: 23px; border-top: 1px solid black">Precio</th>
              </tr>
          </thead>
          <tbody>
              <tr>
              <td style="width: 400px; max-width: 400px; font-size: 23px; text-align:center; border-top: 1px solid black">`+forProductos+`</td>
              <td style="width: 400px; max-width: 400px; font-size: 23px; text-align:center; border-top: 1px solid black">`+forCantidad+`</td>
              <td style="width: 400px; max-width: 400px; font-size: 23px; text-align:center; border-top: 1px solid black">`+forImporte+`</td>
              <td style="width: 400px; max-width: 400px; font-size: 23px; text-align:center; border-top: 1px solid black">`+forPrecio+`</td>
              </tr>  
          </tbody>
      </table>
        <br>
        <br>
        <label style="font-size: 23px;">TOTAL A PAGAR :$`+data.importeCobrar+`</label>
        <br>
        <label style=" text-align: left; font-size: 23px;">EFECTIVO:$ `+importePago+`</label>
        <br>
        <label style=" text-align: left; font-size: 23px;">CAMBIO :$ `+restante+`</label>
        <p style=" text-align: center; font-size: 23px;">**PAGADO**</p>
        <br>
        <label style=" text-align: left; font-size: 23px;"># DE PEDIDO:123456</label>
        <br>
        <label style=" text-align: left; font-size: 23px;">FECHA: `+currentDate+`</label>
        <br>
        <label style=" text-align: left; font-size: 23px;">KOWI PURO SABOR!</label>
        <br>
        <label style=" text-align: left; font-size: 23px;">VISITE WWW.KOWI.COM.MX</label>
        <br>
        <br>
        <br>
        <br>
        <br>
        <label style=" text-align: left; font-size: 23px;">______________________</label>
        <br>
        <label style=" text-align: left; font-size: 23px;">FIRMA CLIENTE</label>
        <br>
        <br>
        <br>
        <br>
        <label style=" text-align: left; font-size: 23px;">______________________</label>
        <br>
        <label style=" text-align: left; font-size: 23px;">FIRMA RUTA</label>
      <br>
      <br>
      <p style=" text-align: center; align-content: center; font-size: 23px;">¡GRACIAS POR SU COMPRA!
  </div>
  </body>
</html>`;

const print = async () => {
    // On iOS/android prints the given html. On web prints the HTML from the current page.
    await Print.printAsync({
      html,
      printerUrl: selectedPrinter?.url, // iOS only
    });
  }

  const printToFile = async () => {
    // On iOS/android prints the given html. On web prints the HTML from the current page.
    const { uri } = await Print.printToFileAsync({
      html
    });
    console.log('File has been saved to:', uri);
    await shareAsync(uri, { UTI: '.pdf', mimeType: 'application/pdf' });
  }

  const selectPrinter = async () => {
    const printer = await Print.selectPrinterAsync(); // iOS only
    setSelectedPrinter(printer);
  }


    return (
        <View style={styles.container}>
         <Text style={{ fontSize: 18, alignSelf: "center", paddingBottom:10 }}>Verificar tus datos</Text>
        <DataTable>
          <DataTable.Header style={styles.headerColor}>
            <DataTable.Title><Text style={{ fontSize: 14,color:'white' }}>Acción</Text></DataTable.Title>
            <DataTable.Title><Text style={{ fontSize: 14,color:'white' }}>Valor</Text></DataTable.Title>
          </DataTable.Header>
          <DataTable.Row>
            <DataTable.Cell>Requiere factura</DataTable.Cell>
            <DataTable.Cell>{data.requiereFactura}</DataTable.Cell>
          </DataTable.Row>
          <DataTable.Row>
            <DataTable.Cell>Tipo de venta</DataTable.Cell>
            <DataTable.Cell>{data.tipoVenta}</DataTable.Cell>
          </DataTable.Row>
          <DataTable.Row>
            <DataTable.Cell>Forma de pago</DataTable.Cell>
            <Picker
            selectedValue={selectedPago}
            style={{ height: 50, width: 150 }}
            onValueChange={(itemValue, itemIndex) => setSelectFormaPago(itemValue)}>
            <Picker.Item label="Efectivo" value="efectivo" />
            <Picker.Item label="Crédito" value="credito" />
          </Picker>
          </DataTable.Row>
          <DataTable.Row>
            <DataTable.Cell>Importe total a cobrar</DataTable.Cell>
            <DataTable.Cell>$ {data.importeCobrar}</DataTable.Cell>
          </DataTable.Row>
          <DataTable.Row>
            <DataTable.Cell>Importe a pagar</DataTable.Cell>
            <DataTable.Cell>
              <View style={styles.contenedor} >
                <TextInput  keyboardType="numeric" onChangeText={setImportePago} value={importePago}/>
              </View>
            </DataTable.Cell>
          </DataTable.Row>
          <DataTable.Row>
            <DataTable.Cell>Importe restante</DataTable.Cell>
            <DataTable.Cell>
            <DataTable.Cell>$ {restante}</DataTable.Cell>
            </DataTable.Cell>
          </DataTable.Row>
        </DataTable>   
        <Text>{restante}</Text> 
        <Text>{selectedPago}</Text> 
        <Button
        style={{
          marginTop: 10,
          width: "80%",
          marginLeft: "10%",
          marginRight: "10%",
          borderRadius: 14,
          borderWidth: 1,
          backgroundColor: "#ff6900",
        }}
        icon="import"
        mode="contained"
        uppercase={false}
        onPress={print}
        loading={loading}>
        <Text style={{ color: "white" }}>Imprimir</Text>
      </Button>
      {Platform.OS === 'ios' &&
        <>
          <View style={styles.spacer} />
          <Button title='Select printer' onPress={selectPrinter}/>
          <View style={styles.spacer} />
          {selectedPrinter ? <Text style={styles.printer}>{`Selected printer: ${selectedPrinter.name}`}</Text> : undefined}
        </>
      }
      </View>
  );
  
} 

const styles = StyleSheet.create({
 
    container: {
        paddingTop: 60,
        paddingHorizontal: 30,
      },
      headerColor:{
        backgroundColor:'#ff6900',
        color:'white'
      }
 
});
