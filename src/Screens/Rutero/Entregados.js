import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Button,DataTable  } from "react-native-paper";

export default function Entregados() {

    const data ={
        id:'3457',
        total:'605',
        fecha:'2021-11-08',
        nombre: 'HOTELES PARADISE DE CULIACAN SA DE CV',
        ruta:'CULIACAN RUTA 1',
        
    }

    return (
      <View style={styles.container}>
      <Text style={{ fontSize: 18, alignSelf: "center", paddingBottom:10 }}>Rutas</Text>
     <DataTable>
       <DataTable.Header style={styles.headerColor}>
         <DataTable.Title><Text style={{ fontSize: 14,color:'white' }}>#</Text></DataTable.Title>
         <DataTable.Title><Text style={{ fontSize: 14,color:'white' }}>Nombre</Text></DataTable.Title>
         <DataTable.Title><Text style={{ fontSize: 14,color:'white' }}>Fecha</Text></DataTable.Title>
         <DataTable.Title><Text style={{ fontSize: 14,color:'white' }}>Total</Text></DataTable.Title>
       </DataTable.Header>

       <DataTable.Row>
         <DataTable.Cell>{data.id}</DataTable.Cell>
         <DataTable.Cell>{data.nombre}</DataTable.Cell>
         <DataTable.Cell>{data.fecha}</DataTable.Cell>
         <DataTable.Cell>{data.total}</DataTable.Cell>
       </DataTable.Row>
       <DataTable.Row>
         <DataTable.Cell>{data.id}</DataTable.Cell>
         <DataTable.Cell>{data.nombre}</DataTable.Cell>
         <DataTable.Cell>{data.fecha}</DataTable.Cell>
         <DataTable.Cell>{data.total}</DataTable.Cell>
       </DataTable.Row>
       <DataTable.Row>
         <DataTable.Cell>{data.id}</DataTable.Cell>
         <DataTable.Cell>{data.nombre}</DataTable.Cell>
         <DataTable.Cell>{data.fecha}</DataTable.Cell>
         <DataTable.Cell>{data.total}</DataTable.Cell>
       </DataTable.Row>
       <DataTable.Row>
         <DataTable.Cell>{data.id}</DataTable.Cell>
         <DataTable.Cell>{data.nombre}</DataTable.Cell>
         <DataTable.Cell>{data.fecha}</DataTable.Cell>
         <DataTable.Cell>{data.total}</DataTable.Cell>
       </DataTable.Row>
       <DataTable.Row>
         <DataTable.Cell>{data.id}</DataTable.Cell>
         <DataTable.Cell>{data.nombre}</DataTable.Cell>
         <DataTable.Cell>{data.fecha}</DataTable.Cell>
         <DataTable.Cell>{data.total}</DataTable.Cell>
       </DataTable.Row>
       
     </DataTable>

    
  
   
   </View>
    )
}


const styles = StyleSheet.create({
 
    container: {
        paddingTop: 30,
        paddingHorizontal: 30,
      },
      headerColor:{
        backgroundColor:'#ff6900',
        color:'white'
      }
});