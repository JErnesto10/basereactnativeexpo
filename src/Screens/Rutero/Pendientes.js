import React, {useState,useEffect} from 'react';
import { View, Text, StyleSheet } from 'react-native'
import { DataTable,IconButton,Colors } from "react-native-paper";
import { color } from 'react-native-reanimated';
import { API_URL } from "../../utils/constants"
import jwtDecode from 'jwt-decode';
import { getTokenApi } from '../../api/token';
import { map } from "lodash";
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { useNavigation } from "@react-navigation/native";


export default function Pendientes() {
  
  const [pedidos, setPedidos] = useState(null);
  const goToPedido = (idpedido) => {
    navigation.push("packing", { idpedido: id });
  };


  useEffect(() => {
    (async () => {
      const token = await getTokenApi();
  
       const url = `${API_URL}/api/despachadosRutero/${jwtDecode(token).sub}`
  
      fetch(url).then((response) => response.json()).then((responseJson) => {
        let dataSource = [];
  
        Object.values(responseJson).forEach(item => {
            dataSource = dataSource.concat(item);
            setPedidos(dataSource);
        });
        console.log(pedidos);
            
    });      
    })();
  }, []);


    const data ={
        id:'3457',
        total:'605',
        fecha:'2021-11-08',
        nombre: 'HOTELES PARADISE DE CULIACAN SA DE CV',
        ruta:'CULIACAN RUTA 1',
        
    }


    return (
        <View style={styles.container}>

<Text style={{ fontSize: 18, alignSelf: "center", paddingBottom:10 }}>Rutas</Text>
        <DataTable.Header style={styles.headerColor}>
           <DataTable.Title><Text style={{ fontSize: 14,color:'white' }}>#</Text></DataTable.Title>
           <DataTable.Title><Text style={{ fontSize: 14,color:'white' }}>Nombre</Text></DataTable.Title>
           <DataTable.Title><Text style={{ fontSize: 14,color:'white' }}>Total</Text></DataTable.Title>
           <DataTable.Title><Text style={{ fontSize: 14,color:'white' }}>Opciones</Text></DataTable.Title>
         </DataTable.Header>
          
        
        {map(pedidos, (pedido) => ( 
          
       <DataTable>

         <DataTable.Row
         key={pedido.id}
         >
           <DataTable.Cell><View style={styles.contenedorRow}><Text>{pedido.id}</Text></View></DataTable.Cell>
           <DataTable.Cell><View style={styles.contenedorRow}><Text>{pedido.name}</Text></View></DataTable.Cell>
           <DataTable.Cell><View style={styles.contenedorRow}><Text>{pedido.importe_total}</Text></View></DataTable.Cell>
           <DataTable.Cell >

    <View style={styles.contenedor} >
        <IconButton
              style={styles.Icon}
              icon="pencil"
              color={Colors.green100}
              size={20}
              onPress={() => console.log('Pressed')}
        />
        <IconButton
        style={styles.Icon}
              icon="play"
              color={Colors.orange100}
              size={20}
              onPress={() => console.log('Pressed')}
        />
     
    </View>
          </DataTable.Cell>
         </DataTable.Row>
        
         
       </DataTable>

))}
    
     
     </View>
    );
}


const styles = StyleSheet.create({
 
    container: {
        paddingTop: 30,
        paddingHorizontal: 30,
      },
      headerColor:{
        backgroundColor:'#ff6900',
        color:'white'
      },
      contenedor:{
        flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
        
      },
      Icon:{
        width: '40%',
        height: 40
        
      },
      contenedorRow:{
        flex: 1,
        justifyContent: 'center',
        alignItems:'center'
        
      }

 
});