import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import Menu from '../components/account/Menu';

export default function Account() {
    return (
        <>
        <ScrollView>
            <Menu />
        </ScrollView>
        </>
    )
}


const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent:"center",
        alignItems: "center",
    }
})