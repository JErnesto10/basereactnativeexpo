import React from 'react'
import { View, Text } from 'react-native'
import { TextInput, Button } from "react-native-paper";
import { formStyle } from '../../styles';

export default function RegisterForm(props) {
  const {changeForm} = props;

    return (
        <View>
        <Button
            mode="text"
            style={formStyle.btnText}
            labelStyle={formStyle.btnTextLabel}
            onPress={changeForm}
          >
            Iniciar Sesión
          </Button>
        </View>
    )
}
