import React from 'react';
import { Alert } from "react-native";
import { List } from 'react-native-paper';
import useAuth from '../../hooks/useAuth';



export default function Menu() {

    const { logout } = useAuth();

    const logoutAccount = () => {
        Alert.alert(
          "Cerrar sesión",
          "¿Estas seguro de que quieres salir de tu cuenta?",
          [
            {
              text: "NO",
            },
            { text: "SI", onPress: logout },
          ],
          { cancelable: false }
        );
      };
    
    return (
        <List.Section>
        <List.Subheader style={{color:"#ff6900", fontSize:17 , marginTop:10}}>Mi cuenta</List.Subheader>
        <List.Item
          title="Cerrar sesión"
          titleStyle={{color: "rgba(0,0,0,0.8)", fontSize:17, fontWeight:'bold'}}
          description="Cierra esta sesion y inicia con otra"
          left={(props) => <List.Icon {...props} icon="logout" />}
          onPress={logoutAccount}
        />
        
      
      </List.Section>
    )
}
